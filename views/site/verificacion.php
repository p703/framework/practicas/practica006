<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    "model"=>$model
]);

?>
<div>
    <?= Html::a('Eliminar',
            ['site/eliminar1','id'=>$model->id],
            [
                'class'=>'btn btn-primary',
            ],
            )
            
    ?>
</div>