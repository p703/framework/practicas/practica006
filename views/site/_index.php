<?php
use yii\helpers\Html;
?>
<div>
    <?= $model->titulo ?>
</div>
<div>
    <?= $model->contenido ?>
</div>

<div>
    <?= Html::a('Eliminar',
            ['site/eliminar','id'=>$model->id],
            [
                'class'=>'btn btn-primary',
                //confirmacion realizada con javascript y una ventana emergente
                /*'data' => [
                    'confirm' => '¿Estás seguro que deseas eliminar el regsitro?',
                    'method' => 'post',
                ],*/
            ],
            )
            
    ?>
</div>